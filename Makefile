#
# Makefile to build all documentation.
#
# Note we use 'asciidoctor' rather than 'asciidoc' since the latter seems to
# be obsolete. Note also that the PDF conversion relies on the 'fop' package.
#
SHELL = /bin/sh

TEXTS  = README.txt get_album.txt report_albumsku.txt update_albums.txt
HTMLS  = $(TEXTS:%.txt=%.html)
PDFS   = $(HTMLS:%.html=%.pdf)

all : $(HTMLS) $(PDFS)

#
# Generic rules for building HTML and PDF
#
%.html : %.txt
	asciidoctor -b html -o $@ $<

%.pdf : %.txt
	a2x -f pdf --fop $<
