<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

    <xsl:template match="/">
	<xsl:for-each select="AllAlbums/Album">
	    <xsl:sort select="artist"/>
	    <xsl:value-of select="concat('Artist: ',artist,'&#xA;')"/>
	    <xsl:value-of select="concat('Album:  ',albumname,'&#xA;')"/>
	    <xsl:value-of select="concat('Genres: ',magnatunegenres,'&#xA;')"/>
	    <xsl:value-of select="concat('Code:   ',albumsku,'&#xA;')"/>
	    <xsl:text>&#xA;</xsl:text>
	</xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
